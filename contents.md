# Contents

- [Credit Bureau](./cb/README.md)
- [데이터 규제 3법 개정 및 금융 빅데이터 개방](./data3/README.md)
  - [데이터 규제 3법 개정](./data3/summary.md)
  - [금융 빅데이터 개방](./data3/credb/summary.md)

