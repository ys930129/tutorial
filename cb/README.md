## Problems
```
- 현재 상태를 파악 
- 공인 신용조회회사(Credit Bureau)외 금융 기관에서 신용점수 산정 방법 및 산정에 쓰이는 데이터 확인
```

## Contents
[1. CB 의 정의](./credit_bureau/summary.md)  

[2. 신용조회회사의 신용평가 모형과 감독](./papers/1.cb_model)

&nbsp;&nbsp;[1) 금융포용](./financial_inclusion)

[3. 신용평가모형(CSS) example](./example)

&nbsp;&nbsp;[1) 심리 신용평가](./psycometric_credit_score)

[4. 신용정보산업 선진화 방안](./report_as_is_kr)

## ToBe
[1. 신용평가점수와 AI]()

### Reference
- [데이터 경제 활성화를 위한 신용정보산업 선진화 방안 - 금융위원회](../../resources/survey/cb/data_eco.pdf)
- [빅데이터와 AI를 활용한 신용평가의 변화시도 - 서울 신용 평가](http://www.scri.co.kr/webzine/file/201806/report_2.pdf)
- [신용조회회사(CB)의 신용평가모형과 감독: 주요국과의 비교 분석을 중심으로](http://www.papersearch.net/thesis/article.asp?key=3698096)
- [해외 금융포용 동향과 시사점](http://www.kiri.or.kr/pdf/전문자료/KIRI_20190510_94647.pdf)